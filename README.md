![MTMT logo](https://img.informer.com/icons/png/128/5216/5216902.png)

# threat-modelling-template

[Microsoft Threat Modelling Tool](https://www.microsoft.com/en-us/download/details.aspx?id=49168) allows the modelling of systems, aiming to identify, and possibly mitigates, vulnerabilities and threats.

## Templates

- `c_its_template.tm7` builds upon the default template by adding C-ITS components. It is aimed to be used for a general scope, and thus typically does not fit detailed systems with for example electronic components and their connections.

## Usage

At the start of Microsoft Threat Modelling Tool, select the template file in `.tm7` format.

## Contact

To contribute to a model, or to propose new ones, open a pull-request. For other remarks or inquiry, open an issue or [contact us by mail](ivan.baeriswyl@rosas.center), mentioning this repository.

## About ROSAS

[ROSAS](https://rosas.center/) is an academic center which supports you in Safety & Reliability, Model-Based Engineering, Cybersecurity and Intelligent Systems.
